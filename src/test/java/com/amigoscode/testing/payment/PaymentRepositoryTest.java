package com.amigoscode.testing.payment;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.assertj.core.api.Assertions.*;

@DataJpaTest(
        properties = {
                "spring.jpa.properties.javax.persistence.validation.mode=none"
        }
)
class PaymentRepositoryTest {

    @Autowired
    private PaymentRepository paymentRepositoryTest;

    @Test
    void itShouldInsertPayment() {
        // Given
        Long paymentID = 1L;
        Payment payment = new Payment(
                paymentID,
                UUID.randomUUID(),
                new BigDecimal("10.00"),
                Currency.SGD,
                "card123",
                "Donation"
                );

        // When
        paymentRepositoryTest.save(payment);

        // Then
        Optional<Payment> paymentOptional = paymentRepositoryTest.findById(paymentID);
        assertThat(paymentOptional)
                .isPresent()
                .hasValueSatisfying(e -> assertThat(e).isEqualToComparingFieldByField(payment));
    }
}