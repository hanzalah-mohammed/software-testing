package com.amigoscode.testing.customer;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.never;

class CustomerRegistrationServiceTest {

    @Mock
    private CustomerRepository customerRepository;

    @Captor
    private ArgumentCaptor<Customer> customerArgumentCaptor;

    private CustomerRegistrationService customerRegistrationServiceTest;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        customerRegistrationServiceTest = new CustomerRegistrationService(customerRepository);
    }

    @Test
    void itShouldSaveNewCustomer() {

        // Given a phone number and a customer
        String phoneNumber = "12321";
        Customer customer = new Customer(UUID.randomUUID(), "Maryam", phoneNumber);

        //...a request
        CustomerRegistrationRequest request = new CustomerRegistrationRequest(customer);

        //...no number passed
        given(customerRepository.selectCustomerByPhoneNumber(phoneNumber)).willReturn(Optional.empty());

        // When
        customerRegistrationServiceTest.registerNewCustomer(request);

        // Then
        then(customerRepository).should().save(customerArgumentCaptor.capture());
        Customer customerArgumentCaptorValue = customerArgumentCaptor.getValue();
        assertThat(customerArgumentCaptorValue).isEqualTo(customer);
    }

    @Test
    void itShouldSaveNewCustomerWhenIdIsNull() {

        // Given a phone number and a customer
        String phoneNumber = "12321";
        Customer customer = new Customer(null, "Maryam", phoneNumber);

        //...a request
        CustomerRegistrationRequest request = new CustomerRegistrationRequest(customer);

        //...no number passed
        given(customerRepository.selectCustomerByPhoneNumber(phoneNumber)).willReturn(Optional.empty());

        // When
        customerRegistrationServiceTest.registerNewCustomer(request);

        // Then
        then(customerRepository).should().save(customerArgumentCaptor.capture());
        Customer customerArgumentCaptorValue = customerArgumentCaptor.getValue();
        assertThat(customerArgumentCaptorValue).isEqualToIgnoringGivenFields(customer, "customerID");
        assertThat(customerArgumentCaptorValue.getCustomerID()).isNotNull();
    }

    @Test
    void itShouldNotSaveWhenCustomerExists() {

        // Given a phone number and a customer
        String phoneNumber = "12321";
        Customer customer = new Customer(UUID.randomUUID(), "Maryam", phoneNumber);

        //...a request
        CustomerRegistrationRequest request = new CustomerRegistrationRequest(customer);

        //...an existing customer with same name & phone number returned
        given(customerRepository.selectCustomerByPhoneNumber(phoneNumber)).willReturn(Optional.of(customer));

        // When
        customerRegistrationServiceTest.registerNewCustomer(request);

        // Then
        then(customerRepository).should(never()).save(any());
    }

    @Test
    void itShouldThrowWhenPhoneNumberIsTaken() {

        // Given a phone number and a customer
        String phoneNumber = "12321";
        Customer customer = new Customer(UUID.randomUUID(), "Maryam", phoneNumber);
        Customer customer2 = new Customer(UUID.randomUUID(), "John", phoneNumber);

        //...a request
        CustomerRegistrationRequest request = new CustomerRegistrationRequest(customer);

        //...an existing customer with different name but same phone number returned
        given(customerRepository.selectCustomerByPhoneNumber(phoneNumber)).willReturn(Optional.of(customer2));

        // When
        // Then
        assertThatThrownBy(() -> customerRegistrationServiceTest.registerNewCustomer(request))
                .isInstanceOf(IllegalStateException.class)
                .hasMessageContaining(String.format("phone number [%s] is taken", phoneNumber));

        // Finally
        then(customerRepository).should(never()).save(any());

    }
}