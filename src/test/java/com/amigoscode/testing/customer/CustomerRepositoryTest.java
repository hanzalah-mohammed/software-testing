package com.amigoscode.testing.customer;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.DataIntegrityViolationException;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.assertj.core.api.Assertions.*;

@DataJpaTest(
        properties = {
                "spring.jpa.properties.javax.persistence.validation.mode=none"
        }
)
class CustomerRepositoryTest {

    @Autowired
    private CustomerRepository customerRepositoryTest;


    @Test
    void itShouldSaveNewCustomer() {
        // Given
        UUID id = UUID.randomUUID();
        Customer customer = new Customer(id, "Sample", "123456");

        // When
        customerRepositoryTest.save(customer);

        // Then
        Optional<Customer> customerOptional = customerRepositoryTest.findById(id);
        assertThat(customerOptional)
                .isPresent()
                .hasValueSatisfying(c ->
                        assertThat(c).isEqualToComparingFieldByField(customer));
    }

    @Test
    void itShouldNotSaveWhenNameIsNull() {
        // Given
        UUID id = UUID.randomUUID();
        Customer customer = new Customer(id, null, "123456");

        // When
        // Then
        assertThatThrownBy(() -> customerRepositoryTest.save(customer))
                .isInstanceOf(DataIntegrityViolationException.class)
                .hasMessageContaining("not-null property references a null or transient value");
    }

    @Test
    void itShouldNotSaveWhenPhoneNumberIsNull() {
        // Given
        UUID id = UUID.randomUUID();
        Customer customer = new Customer(id, "Abel", null);

        // When
        // Then
        assertThatThrownBy(() -> customerRepositoryTest.save(customer))
                .isInstanceOf(DataIntegrityViolationException.class)
                .hasMessageContaining("not-null property references a null or transient value");
    }

    @Test
    void itShouldSelectCustomerByPhoneNumber() {

        // Given
        String phone = "123456";
        Customer customer = new Customer(UUID.randomUUID(), "Sample", phone);

        // When
        customerRepositoryTest.save(customer);

        // Then
        Optional<Customer> customerOptional = customerRepositoryTest.selectCustomerByPhoneNumber(phone);
        assertThat(customerOptional)
                .isPresent()
                .hasValueSatisfying(c ->
                        assertThat(c).isEqualToComparingFieldByField(customer));
    }

    @Test
    void itShouldNotSelectCustomerByPhoneNumberWhenNumberNotExists() {

        // Given
        String phone = "123456";

        // When
        Optional<Customer> customerOptional = customerRepositoryTest.selectCustomerByPhoneNumber(phone);

        // Then
        assertThat(customerOptional).isNotPresent()
;    }
}